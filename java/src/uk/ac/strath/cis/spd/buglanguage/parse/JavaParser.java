package uk.ac.strath.cis.spd.buglanguage.parse;

/**
 * See LICENCE_BSD for licensing information.
 *
 * Copyright Steven Davies 2012
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeParameter;
import org.eclipse.jface.text.Document;

public class JavaParser {
        private int methodId;
        private PrintWriter methodIds;
        private List<Pattern> filePatterns;

        public JavaParser(File methodIds, String patterns) throws IOException{
            methodId = -1;
            this.methodIds = new PrintWriter(FileUtils.openOutputStream(methodIds));
            filePatterns = new ArrayList<Pattern>();
            for(String pattern: patterns.split(",")){
                filePatterns.add(Pattern.compile(pattern.trim()));
            }
        }

        private String createNextMethodId(String methodName){
            methodId++;
            methodIds.println(methodId + "\t" + methodName);
            return String.valueOf(methodId);
        }

	@SuppressWarnings("unchecked")
	public MethodResult[] extractMethods(String input) {
		List<MethodResult> results = new ArrayList<MethodResult>();

		Document document = new Document(input);
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setSource(document.get().toCharArray());
		CompilationUnit unit = (CompilationUnit) parser.createAST(null);

		// to get the imports from the file
		List<TypeDeclaration> types = unit.types();
		for (TypeDeclaration type : types) {
			PackageDeclaration typePackage = unit.getPackage();
			String packageName = typePackage != null ? typePackage.getName()
					.toString() : null;
			results.addAll(getMethods(input, type, packageName));
		}

		return results.toArray(new MethodResult[results.size()]);
	}

        private void getTypeParameters(List<TypeParameter> parameters, Map<String, String> typeVariables){
            for(TypeParameter parameter: parameters){
                String type = "Object";
                if(!parameter.typeBounds().isEmpty()){
                    type = parameter.typeBounds().get(0).toString();
                }
                typeVariables.put(parameter.getName().toString(), type);
            }
        }                                                                                      

	@SuppressWarnings("unchecked")
	private List<MethodResult> getMethods(String source, TypeDeclaration t,
			String prefix) {
		String typeName = t.getName().toString();
		if (prefix != null) {
			typeName = prefix + "." + typeName;
		}

                Map<String, String> classTypeVariables = new HashMap<String, String>();
                getTypeParameters(t.typeParameters(), classTypeVariables);

		List<MethodResult> results = new ArrayList<MethodResult>();

		MethodDeclaration[] methods = t.getMethods();
		for (MethodDeclaration methodDeclaration : methods) {
                        Map<String, String> typeVariables = new HashMap<String, String>(classTypeVariables);
                        getTypeParameters(methodDeclaration.typeParameters(), typeVariables);

			StringBuilder signature = new StringBuilder();
			signature.append(typeName);
			signature.append(".");
			signature.append(methodDeclaration.getName());
			signature.append("(");
			List<SingleVariableDeclaration> parameters = methodDeclaration
					.parameters();
			if (!parameters.isEmpty()) {
				for (SingleVariableDeclaration parameter : parameters) {
                                        String type = parameter.getType().toString();
                                        if(typeVariables.containsKey(type)){
                                            type = typeVariables.get(type);
                                        }
                                        if(type.contains("<")){
                                            type = type.substring(0, type.indexOf("<"));
                                        }
                                        if(type.contains(".")){
                                            type = type.substring(type.lastIndexOf(".") + 1);
                                        }
					signature.append(type);
					if (parameter.isVarargs()) {
						signature.append("[]");
					}
                                        for(int i=0; i<parameter.getExtraDimensions();i++){
                                            signature.append("[]");
                                        }
					signature.append(",");
				}
				signature.deleteCharAt(signature.length() - 1);
			}
			signature.append(")");

			Block body = methodDeclaration.getBody();
			if (body != null) {
				int startLine = 1 + StringUtils.countMatches(source.substring(0, methodDeclaration.getStartPosition()), "\n");
				String bodyText = source.substring(body.getStartPosition(),
						body.getStartPosition() + body.getLength());
				int endLine = startLine + StringUtils.countMatches(source.substring(methodDeclaration.getStartPosition(), body.getStartPosition() + body.getLength()), "\n");
				bodyText = bodyText.trim();
				if(bodyText.startsWith("{")) bodyText = bodyText.substring(1);
				if(bodyText.endsWith("}")) bodyText = bodyText.substring(0,bodyText.length()-1);
				bodyText = bodyText.trim();
//				if(bodyText.length()>0){
					MethodResult result = new MethodResult(signature.toString(), startLine, endLine, bodyText);
					results.add(result);
//				}
			}
		}

		TypeDeclaration[] innerTypes = t.getTypes();
		for (TypeDeclaration innerType : innerTypes) {
			results.addAll(getMethods(source, innerType, typeName));
		}
		return results;
	}

	private void writeMethodsForFile(File dst, File file) throws IOException {
		MethodResult[] methods = extractMethodsForFile(file);
		for (MethodResult methodResult : methods) {
			BufferedWriter writer = null;
			try{
				File destination = new File(dst, createNextMethodId(methodResult.getName()));
				writer = new BufferedWriter(new FileWriter(destination));
				writer.write(methodResult.getBody());
			}
                        catch(FileNotFoundException e){
                            System.out.println("Could not write file for " + methodResult.getName());
                        }
			finally{
				if(writer != null){
					writer.close();
				}
			}
		}
	}

	public MethodResult[] extractMethodsForFile(File file) throws FileNotFoundException, IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder builder = new StringBuilder();
		char[] chars = new char[1024];
		int numRead;
		while ((numRead = reader.read(chars)) > -1) {
			builder.append(String.valueOf(chars).substring(0, numRead));
		}
		MethodResult[] methods = extractMethods(builder.toString());
		return methods;
	}

	private void writeFiles(File root, File src, File dst) throws IOException {
		File[] listFiles = src.listFiles();
		for (File file : listFiles) {
			if (file.isDirectory()) {
				if (file.getName().equals(".svn")
						|| file.getName().equals(".git")) {
					continue;
				}
				writeFiles(root, file, dst);
			} else {
				if (!shouldParseFile(root, file)) {
					continue;
				}
				writeMethodsForFile(dst, file);
			}
		}
	}

        public boolean shouldParseFile(File root, File file){
            String path = root.toURI().relativize(file.toURI()).getPath();
            for(Pattern pattern: filePatterns){
                if(pattern.matcher(path).matches()){
                    return true;
                }
            }
            System.out.println("No match for " + path);
            return false;
        }

        public void cleanup(){
            methodIds.close();
        }

	public static void main(String[] args) throws IOException {
		Properties config = new Properties();
		config.load(new FileReader(args[0]));

 		File corpus = new File(config.getProperty("corpus"));
                File methodIds = new File(config.getProperty("method_ids"));
                String patterns = config.getProperty("file_patterns");
		JavaParser parser = new JavaParser(methodIds, patterns);
		corpus.mkdir();
		File[] files = corpus.listFiles();
		for (File file : files) {
			file.delete();
		}

                File root = new File(config.getProperty("working_copy"));
                try{
                    parser.writeFiles(root, root, corpus);
                }
                finally{
                    parser.cleanup();
                }
	}
}
