/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.links.git;

import uk.ac.strath.cis.spd.buglanguage.links.LogInputListener;
import uk.ac.strath.cis.spd.buglanguage.links.LogInputListener.Revision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GitLogInputListener implements LogInputListener
{
	private List<Revision> revisions;
	private Revision revision;
	private Pattern bugPattern;
	private static final Pattern revisionPattern = Pattern.compile("^Hash: ([0-9a-f]+)$");
	
	public GitLogInputListener(String bugPattern)
	{
		this.revisions = new ArrayList<Revision>();
		this.bugPattern = Pattern.compile(bugPattern);
	}

	@Override
	public void processLine(String line)
	{
		Matcher matcher = revisionPattern.matcher(line);
		if(matcher.matches()) {
			revision = new Revision(matcher.group(1)); 
		}
		else if (revision != null) {
			matcher = bugPattern.matcher(line);
			if(matcher.find()) {
				do{
					revision.addBug(matcher.group(1), line);
				} while(matcher.find());
				
				if(revisions.isEmpty() || !revisions.get(revisions.size()-1).equals(revision)){
					revisions.add(revision);
				}
			}
		}
	}

	public List<Revision> getRevisions()
	{
		return revisions;
	}
}
