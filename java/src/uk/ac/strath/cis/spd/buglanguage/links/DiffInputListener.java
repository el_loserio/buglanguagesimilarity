/**
 * See LICENCE_BSD for licensing information
 *
 * Coyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.links;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class DiffInputListener implements InputListener
{
	public class Diff
	{
		private String file;
		private int oldFrom;
		private int oldTo;
		private int newFrom;
		private int newTo;
		
		public Diff(String file, String oldFrom, String oldLength, String newFrom, String newLength)
		{
			super();
			this.file = file;
			this.oldFrom = Integer.parseInt(oldFrom);
			this.oldTo = this.oldFrom + parseInt(oldLength);
			this.newFrom = Integer.parseInt(newFrom);
			this.newTo = this.newFrom + parseInt(newLength);
		}
		
		private int parseInt(String in) {
			if(in == null) {
				return 0;
			}
			return Integer.parseInt(in);
		}

		public String getFile()
		{
			return file;
		}

		public int getFrom(boolean old)
		{
			return old ? oldFrom : newFrom;
		}

		public int getTo(boolean old)
		{
			return old ? oldTo : newTo;
		}
	}

	private List<Diff> changes;
	private String file;

	public DiffInputListener()
	{
		this.changes = new ArrayList<Diff>();
	}

	@Override
	public void processLine(String line)
	{
		Matcher matcher = getFilePattern().matcher(line);
		if(matcher.matches()) {
			file = matcher.group(1);
		}
		else {
			matcher = getHunkPattern().matcher(line);
			if(matcher.matches()) {
				changes.add(new Diff(file, matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4)));
			}
		}
	}

	public List<Diff> getChanges()
	{
		return changes;
	}

        protected abstract Pattern getFilePattern();
        protected abstract Pattern getHunkPattern();
}
