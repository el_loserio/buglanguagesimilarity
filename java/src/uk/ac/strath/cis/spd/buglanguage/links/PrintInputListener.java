/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.links;

public class PrintInputListener implements InputListener
{
	@Override
	public void processLine(String line)
	{
		System.out.println(line);
	}
}
