/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.links;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface LogInputListener extends InputListener
{
	public static class Revision
	{
		private String revision;
		private Map<String, String> bugs;
		
		public Revision(String revision)
		{
			this.revision = revision;
			this.bugs = new HashMap<String,String>();
		}
		
		public void addBug(String bug, String line)
		{
			bugs.put(bug, line);
		}

		public String getRevision()
		{
			return revision;
		}

		public List<String> getBugs()
		{
			return new ArrayList<String>(bugs.keySet());
		}
	}

	List<Revision> getRevisions();
}
