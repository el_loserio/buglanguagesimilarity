/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies 2012
 */
package uk.ac.strath.cis.spd.buglanguage.links;

public interface InputListener
{
	void processLine(String line);
}
