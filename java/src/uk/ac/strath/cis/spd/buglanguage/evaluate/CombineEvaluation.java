package uk.ac.strath.cis.spd.buglanguage.evaluate;

/*
 * See LICENCE_BSD for licensing information.
 *
 * Copyright Steven Davies 2012
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils; 

import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class CombineEvaluation
{
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final ProcessBuilder gitHash = new ProcessBuilder("git", "log", "-1", "--pretty=format:%H");
    private static final ProcessBuilder gitStats = new ProcessBuilder("git", "diff", "--numstat");
                            
    private List<String> names;
    private List<File> inputFiles;
    private File resultsDir, relevant;
    private List<Double> powers;
    private double maxValue;

    public CombineEvaluation(String config, String output, List<String> names) throws Exception
    {
        Properties props = new Properties();
        props.load(new FileReader(config));
        relevant = new File(props.getProperty("results"), "relevant");
        resultsDir = new File(props.getProperty("results"), output);
        resultsDir.mkdir();
        FileUtils.cleanDirectory(resultsDir);
        this.names = names;
        maxValue = 0;
        powers = new ArrayList<Double>();
        inputFiles = new ArrayList<File>();
        for(int i=0; i<names.size(); i++){
            inputFiles.add(new File(props.getProperty("results"), names.get(i)));
            double power = Math.pow(10, i);
            powers.add(power);
            maxValue += power;
        }
    }
    
    private String[] nextLine(Iterator<String> iterator){
        if(iterator == null){
            return null;
        }
        String line;
        do{
            line = iterator.next();
        } while (line.startsWith("#"));
        return line.trim().split("\\s+");

    }

    public void evaluate() throws Exception
    {
        String[] bugs = relevant.list();
        Arrays.sort(bugs, new Comparator<String>() {
            public int compare(String o1, String o2){
                return Integer.valueOf(o1).compareTo(Integer.valueOf(o2));
            }
        });

        for(String bugId: bugs){
            Iterator<String> relevantMethods = FileUtils.lineIterator(new File(relevant, bugId));
            List<Iterator<String>> allValues = new ArrayList<Iterator<String>>();
            for(File inputFile: inputFiles){
                try{
                    allValues.add(FileUtils.lineIterator(new File(inputFile, bugId)));
                }
                catch(FileNotFoundException e){
                    System.out.println(e.getMessage());
                }
            }
            PrintWriter results = new PrintWriter(FileUtils.openOutputStream(new File(resultsDir, bugId)));
            try{
                writeMetaData(results);

                while(relevantMethods.hasNext()){
                    String[] parts = nextLine(relevantMethods);
                    String methodId = parts[0];
                    double result = 0;

                    for(int i=0; i<allValues.size(); i++){
                        String[] parts2 = nextLine(allValues.get(i)); 
                        if(parts2 != null){
                            assert parts2[0] == methodId;  
                            result += Double.parseDouble(parts2[1]) * powers.get(i);
                        }
                    }
                    
                    result /= maxValue;
                    print(results, methodId, result);
                }
            }
            finally{
                results.close();
            }
        }
    }

    protected void writeMetaData(PrintWriter writer)
    {
        print(writer, "#", "Attributes", 7);
        print(writer, "#", "Source", getClass().getCanonicalName());
        print(writer, "#", "Timestamp", dateFormat.format(new Date()));
        print(writer, "#", "Version", getGitVersion());
        print(writer, "#", "Changes", getGitChanges());
        print(writer, "#", "Database", getDbVersion());
        print(writer, "#", "Seed", "N/A");
    }
    
    private String getGitVersion() {
        try
        {
            Process process = gitHash.start();
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            return reader.readLine();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "Unavailable";
        }
    }
    
    private String getGitChanges() {
        try
        {
            Process process = gitStats.start();
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = reader.readLine();
            int[] changes = new int[3];
            while(line != null) {
                String[] parts = line.split("\\s+", 3);
                if(!parts[2].startsWith("data/")) {
                    try
                    {
                        changes[0] += Integer.parseInt(parts[0]);
                        changes[1] += Integer.parseInt(parts[1]);
                    }
                    catch (NumberFormatException e)
                    {
                        // Do nothing
                    }
                    changes[2] += 1;
                }
                line = reader.readLine();
            }
            
            if(changes[2] == 0) {
                return "No changes";
            }
            
            return String.format("%s files changed, %s insertions(+), %s deletions(-)", changes[2], changes[0], changes[1]);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "Unavailable";
        }
    }

    private String getDbVersion()
    {
            return "NA";
    }

    private void print(PrintWriter writer, Object... objects){
        writer.println(StringUtils.join(objects, "\t"));
    }

    public static void main(String[] args) throws Exception{
        CombineEvaluation evaluation = new CombineEvaluation(args[0], "combined", Arrays.asList("Tfidf", "similarity"));
        evaluation.evaluate();
        evaluation = new CombineEvaluation(args[0], "bug_count_cmb", Arrays.asList("Tfidf", "bug_count"));
        evaluation.evaluate();
    }
}
