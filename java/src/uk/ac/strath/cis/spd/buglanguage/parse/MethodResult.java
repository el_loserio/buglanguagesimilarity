package uk.ac.strath.cis.spd.buglanguage.parse;

/**
 * See LICENCE_BSD for licensing information
 *
 * Copyright Steven Davies
 */

public class MethodResult
{
	private String name;
	private String body;
	private int startLine;
	private int endLine;

	public MethodResult(String name, int startLine, int endLine, String body)
	{
		super();
		this.name = name;
		this.startLine = startLine;
		this.endLine = endLine;
		this.body = body;
	}

	public String getName()
	{
		return name;
	}

	public String getBody()
	{
		return body;
	}

	public int getStartLine() {
		return startLine;
	}

	public int getEndLine() {
		return endLine;
	}
}
