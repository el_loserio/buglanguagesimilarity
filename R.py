#!/usr/bin/python

import os
import subprocess
import ConfigParser

config_path = os.path.join('build', 'build.properties')
config = ConfigParser.SafeConfigParser()
config.optionxform = str
if not config.read(config_path):
    raise IOError('Could not read ' + config_path)
config = dict(config.items('config'))
command = ['R']
env = os.environ.copy()
env['R_LOCAL_LIB'] = config['r_lib_dir']
env['R_RESULTS'] = config['results_dir']
cwd = os.path.join('latex', 'paper')
subprocess.call(command, env=env, cwd=cwd, shell=True)
