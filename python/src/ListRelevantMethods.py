'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from os import path, listdir, mkdir
from shutil import rmtree
import errno
from Utils import get_config
from VersionUtils import write_metadata
import sys

def print_methods(methods, method_ids, results):
    for f in listdir(methods):
        bug_id = f.replace('GoldSet', '').replace('.txt', '')
        with open(path.join(methods, f)) as bug:
            relevant = set([l.strip() for l in bug])

        with open(path.join(results, bug_id), 'w', 1) as out_file:
            write_metadata(out_file, __file__)
            for method_id, method in method_ids:
                expected = str(int(method in relevant))
                out_file.write('\t'.join((method_id, expected)))
                out_file.write('\n')

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    method_ids = []
    with open(config['method_ids']) as f:
        for line in f:
            method_id, method = line.strip().split()
            method_ids += [(method_id, method)]
                
    results = path.join(config['results'], 'relevant')
    rmtree(results, ignore_errors=True)
    try:
        mkdir(results)
    except OSError as e:
        if e.errno == errno.EEXIST: pass
        else: raise

    print_methods(config['methods'], method_ids, results)
