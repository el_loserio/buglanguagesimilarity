'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from os import path, listdir
import sys
from collections import defaultdict
from shutil import rmtree
from Utils import get_config
from VersionUtils import write_metadata

def check_bug(bug_id, results, method_results):
    methods = set()
    with open(path.join(results, 'relevant', bug_id)) as f:
        for line in f:
            if line.startswith('#'): continue
            method_id, relevant = line.split()
            if relevant == '1':
                method_results[method_id][0] += 1
                methods.add(method_id)

        with open(path.join(results, 'similarity', bug_id)) as f:
            for line in f:
                if line.startswith('#'): continue
                method_id, predicted = line.split()
                if predicted == '1.0':
                    if method_id in methods:
                        method_results[method_id][1] += 1
                    else:
                        method_results[method_id][2] += 1

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    results = config['results']

    method_results = defaultdict(lambda: [0,0,0])
    for bug_id in listdir(path.join(results, 'relevant')):
        check_bug(bug_id, results, method_results)
    with open(path.join(results, 'similarity.methods'), 'w', 1) as o:
        write_metadata(o, __file__)
        for method_id, (bugs, tp, fp) in method_results.items():
            o.write('\t'.join((method_id, str(bugs), str(tp), str(fp))))
            o.write('\n')

