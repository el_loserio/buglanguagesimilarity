'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from Utils import get_config
import codecs
from os import path, listdir, mkdir
from shutil import rmtree
import sys
import warnings
from urllib2 import urlopen, URLError
from BeautifulSoup import BeautifulSoup, BeautifulStoneSoup

def parse_bugzilla(parser):
    doc = parser.find('form', id='changeform')
    if doc:
        summary = doc.find('span', id='short_desc_nonedit_display').getText(' ')
        description = doc.find('div', 'bz_first_comment').find('pre').getText(' ')
        
        return summary, description
    return None

def parse_sourceforge(parser):
    doc = parser.find('div', id='doc3')
    if doc and 'view_artifact' in doc['class']:
        main = doc.find('div', id='yui-main')
        summary = main.find('div', 'yui-u').findAll('span')[1].getText(' ')
        description = main.find('div', 'yui-g').find('p').getText(' ')
        
        return summary, description
    return None

def parse_tigris(parser):
    doc = parser.find('div', id='issuezilla')
    if doc:
        table = doc.find('table', 'axial')
        if table:
            headers = table.findAll('th')
            for header in headers:
                if header.getText(' ') == '* Summary:':
                    summary = header.nextSibling.nextSibling.getText(' ')
                    break
            description = doc.find('div', id='desc1').find('pre').getText(' ')
            
            return summary, description
    return None

def parse_trac(parser):
    doc = parser.find('div', id='ticket')
    if doc:
        summary = doc.find('h2', 'summary').getText(' ')
        description = doc.find('div', 'description').find('div', 'searchable').getText(' ')
        
        return summary, description
    return None

def parse_jira(parser):
    doc = parser.find('body', id='jira')
    if doc:
        summary = doc.find(['h1', 'h2'], id=['issue_header_summary', 'summary-val'])
        if summary:
            summary = summary.getText(' ')
            description = doc.find('div', id=['issue-description', 'description-val'])
            if description:
                description = description.getText(' ')
            
            return summary, description
    return None

def write_bugs(config, bugs_dir, links_dir):
    rmtree(bugs_dir, True)
    mkdir(bugs_dir)
    for s in listdir(links_dir):
        if s.startswith('GoldSet'):
            bug_id = s.replace('GoldSet', '').replace('.txt', '')
            try:
                url = config['tracker'] % (bug_id,)
                parser = BeautifulSoup(urlopen(url), convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
                if config['tracker_type'] == 'sourceforge':
                    parsed = parse_sourceforge(parser)
                elif config['tracker_type'] == 'trac':
                    parsed = parse_trac(parser)
                elif config['tracker_type'] == 'jira':
                    parsed = parse_jira(parser)
                elif config['tracker_type'] == 'tigris':
                    parsed = parse_tigris(parser)
                elif config['tracker_type'] == 'bugzilla':
                    parsed = parse_bugzilla(parser)
                else:
                    print 'Invalid tracker type: ' + config['tracker_type']
                    return
                if parsed:
                    summary, description = parsed
                    with codecs.open(path.join(bugs_dir, 'ShortDescription' + bug_id + '.txt'), 'w', 'utf8') as o:
                        o.write(summary)
                    with codecs.open(path.join(bugs_dir, 'LongDescription' + bug_id + '.txt'), 'w', 'utf8') as o:
                        o.write(description if description else summary)
                else:
                    print 'Could not find data for bug %s' % (bug_id, )
            except URLError as e:
                print e
            except:
                print 'Error parsing bug', bug_id
                raise

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    warnings.simplefilter('error')
    write_bugs(config, config['old_bugs'], config['old_methods'])
    if config['partition'] == 'true':
        write_bugs(config, config['bugs'], config['methods'])
