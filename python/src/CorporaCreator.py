'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from Utils import get_config
from gensim.models import TfidfModel, LdaModel, LsiModel
import logging
import sys
from CorpusUtils import CorpusUtils

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def create_tfidf(corpus_utils, corpus, dictionary):
    tfidf = corpus_utils.get_model(TfidfModel, 'Tfidf', corpus, dictionary)
    corpus_utils.save_model(tfidf, 'Tfidf')
    index = corpus_utils.get_index('Tfidf', tfidf[corpus], num_features=len(dictionary))
    corpus_utils.save_index(index, 'Tfidf')

def create_model(prefix, model_class, corpus_utils, corpus, dictionary, num_topics, **kwargs):
    key = prefix + ' ' + str(num_topics)
    model = corpus_utils.get_model(model_class, key, corpus, id2word=dictionary, num_topics=num_topics, **kwargs)
    corpus_utils.save_model(model, key)
    index = corpus_utils.get_index(key, model[corpus], num_features=num_topics)
    corpus_utils.save_index(index, key)
    
if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    corpus_utils = CorpusUtils(config)
    corpus_utils.recreate_all()
    
    corpus, dictionary = corpus_utils.get_corpus_and_dictionary(config)
    
    corpus_utils.save_dictionary(dictionary)
    
    corpus_utils.save_corpus(corpus, dictionary)
    
    index = corpus_utils.get_index('corpus', corpus, num_features=len(dictionary))
    corpus_utils.save_index(index, 'corpus')
    
    create_tfidf(corpus_utils, corpus, dictionary)

    for num_topics in [50,100,200,300,500]: 
        create_model('Lda', LdaModel, corpus_utils, corpus, dictionary, num_topics, passes=10)
        create_model('Lsi', LsiModel, corpus_utils, corpus, dictionary, num_topics)
