'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012

'''

from operator import itemgetter
from os import path, listdir, mkdir
from shutil import rmtree
import sys
import errno
from Utils import get_config
from VersionUtils import write_metadata

def create_files(results, names):
    out_files = {}
    for name in names:
        sorted_results = path.join(results, name + '.sorted')
        rmtree(sorted_results, ignore_errors=True)
        try:
            mkdir(sorted_results)
        except OSError as e:
            if e.errno == errno.EEXIST: pass
            else: raise

        out_file = open(path.join(results, name + '.frm'), 'w', 1)
        write_metadata(out_file, __file__)
        out_files[name] = out_file
    return out_files

def find_frm(bug_id, out_files, results, names):
    methods = set()
    with open(path.join(results, 'relevant', bug_id)) as f:
        for line in f:
            if line.startswith('#'): continue
            method_id, relevant = line.split()
            if relevant == '1':
                methods.add(method_id)

    for name in names:
        method, position = None, None
        try:
            scores = []
            with open(path.join(results, name, bug_id)) as f:
                for line in f:
                    if line.startswith('#'): continue
                    method_id, score = line.split()
                    scores += [(method_id, float(score), method_id not in methods)]
            with open(path.join(results, name + '.sorted', bug_id), 'w') as o:
                for i, (method_id, _, irrelevant) in enumerate(sorted(scores, key=itemgetter(1, 2), reverse=True)):
                    o.write('\t'.join((method_id, str(i))))
                    o.write('\n')
                    if not method and not irrelevant:
                        method, position = method_id, str(i)
        except IOError as e:
            print e
        if not method:
            method, position = 'NA', 'NA'

        out_files[name].write('\t'.join((bug_id, method, position)))
        out_files[name].write('\n')

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    results = config['results']

    names = ['Tfidf', 'bug_count_cmb', 'bug_count', 'similarity', 'combined']
    for prefix in ['Lda', 'Lsi']:
        for num_topics in [50, 100, 200, 300, 500]:
            names += [prefix + ' ' + str(num_topics)]

    out_files = create_files(results, names)
    for bug_id in listdir(path.join(results, 'relevant')):
        find_frm(bug_id, out_files, results, names)
    for out_file in out_files.values():
        out_file.close()

