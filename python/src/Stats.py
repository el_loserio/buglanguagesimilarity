'''
See LICENCE_BSD for licensing information

@author: Steven Davies 2012
'''

from Utils import get_config
from VersionUtils import write_metadata
from collections import defaultdict
from os import listdir, mkdir, path
import errno
import sys
import errno
import itertools
    
def count_bugs(src, processed, bugs_per_method):
    for f in listdir(src):
        if f not in processed:
            processed += [f]
            with open(path.join(src, f)) as bug:
                for line in bug:
                    bugs_per_method[line.strip()].add(f)

if __name__ == '__main__':
    config = get_config(sys.argv[1], 'config')
    results = config['results']
    corpus = config['corpus']
    methods = config['methods']
    old_methods = config['old_methods']
    bugs = config['bugs']
    old_bugs = config['old_bugs']

    num_bugs = len(listdir(bugs))/2
    num_train_bugs = len(listdir(old_bugs))/2
    num_methods = len(listdir(corpus))

    bugs_per_method = defaultdict(set)
    processed = []
    count_bugs(methods, processed, bugs_per_method)

    multiple_bugs = set(itertools.chain(*[v for v in bugs_per_method.values() if len(v) > 1]))

    count_bugs(old_methods, processed, bugs_per_method)
    num_bugs_per_method = dict([(k,len(v)) for k,v in bugs_per_method.items()])

    try:
        mkdir(config['results'])
    except OSError as e:
        if e.errno == errno.EEXIST: pass
        else: raise

    with open(path.join(results, 'Stats.tsv'), 'w') as out:
        write_metadata(out, __file__, None, None)
        out.write('\t'.join(('Evaluated bugs', str(num_bugs))) + '\n')
        out.write('\t'.join(('Training bugs', str(num_train_bugs))) + '\n')
        out.write('\t'.join(('Applicable bugs', str(len(multiple_bugs)))) + '\n')
        out.write('\t'.join(('Total methods', str(num_methods))) + '\n')
        out.write('\t'.join(('Start revision', config['start_revision'])) + '\n')
        out.write('\t'.join(('End revision', config['end_revision'])) + '\n')
        for k, v in num_bugs_per_method.items():
            out.write('\t'.join((k, str(v))) + '\n')
