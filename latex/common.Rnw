\title{Using bug report similarity to enhance bug localisation}
\newcommand{\code}[1]{\texttt{#1}}

<<echo=FALSE>>=
    options(device= function(...) { .Call('R_GD_nullDevice', PACKAGE = 'grDevices') })

    library(Hmisc)
    library(reshape)
    library(xtable)

    source(paste(Sys.getenv('R_LOCAL_LIB'), 'tables.R', sep='/'))
@

<<cache=TRUE>>=
    results_dir <- Sys.getenv('R_RESULTS')

    meta<-data.frame(file=character(0), attribute=character(0), value=character(0))
    read_file<-function(col.names, ...){
        filename<-paste(results_dir, ..., sep='')
        header<-read.table(filename, nrows=1, sep='\t', comment.char='', strip.white=TRUE)
        if(header[1,'V2']=='Attributes'){
            metadata<-read.table(filename, nrows=header[1,'V3'], sep='\t', comment.char='', strip.white=TRUE)
            metadata$file<-filename
            meta<<-rbind(meta, metadata[,c('file', 'V2', 'V3')])
        }
        read.table(filename, col.names=col.names, sep='\t', comment.char='#', strip.white=TRUE, stringsAsFactors=FALSE)
    }

    max_bugs<-0
    read_stats <- function(project, granularity, filename){
        results <- read_file(c('method', 'num_bugs'), project, '/', granularity, '/', filename, '.tsv')
        num_bugs <- results[results$method=='Evaluated bugs','num_bugs']
        num_train_bugs <- results[results$method=='Training bugs','num_bugs']
        num_applicable_bugs <- results[results$method=='Applicable bugs','num_bugs']
        num_methods <- results[results$method=='Total methods','num_bugs']
        start_revision <- results[results$method=='Start revision','num_bugs']
        end_revision <- results[results$method=='End revision','num_bugs']
        bugs_per_method <- results[!results$method %in% c('Total methods', 'Evaluated bugs', 'Applicable bugs', 'Training bugs', 'Start revision', 'End revision'),]
        bugs_per_method$num_bugs<-as.numeric(bugs_per_method$num_bugs)
        max_bugs <<- max(max_bugs, max(as.integer(names(table(bugs_per_method$num_bugs)))))
        list(num_bugs=num_bugs, num_train_bugs=num_train_bugs, num_applicable_bugs=num_applicable_bugs,num_methods=num_methods, start_rev=start_revision, end_rev=end_revision, bugs_per_method=bugs_per_method)
    }

    read_method_ids <- function(project, granularity){
        results <- read_file(c('method', 'method_name'), project, '/', granularity)
        ret <- results$method_name
        names(ret) <- results$method
        ret
    }

    read_predictions <- function(project, granularity){
        read_file(c('bug', 'predictions'), project, '/', granularity, '/', 'similarity.pred')
    }

    get_frm <- function(project, granularity, filename, num_methods){
        results <- read_file(c('bug', 'method', 'position'), project, '/', granularity, '/', filename, '.frm')
        ret <- results$position + 1
        names(ret) <- paste(results$bug, results$method, sep=' - ')
        ret[is.na(ret)] <- num_methods + 1
        ret
    }

    get_bugs_per_method <- function(stats){
        one_bug <- sum(stats$bugs_per_method$num_bugs == 1)
        multiple_bugs <- sum(stats$bugs_per_method$num_bugs > 1)
        no_bugs <- stats$num_methods - one_bug - multiple_bugs
        data.frame(`Methods with 0 bugs`=no_bugs, `Methods with 1 bug`=one_bug, `Methods with $\\geqslant$2 bugs`=multiple_bugs, check.names=FALSE)
    }

    get_project <- function(name, granularity, id_granularity){
        stats <- read_stats(name, granularity, 'Stats')
        method_ids <- read_method_ids(name, id_granularity)
        num_methods <- as.numeric(stats$num_methods)
        frm <- get_frm(name, granularity, 'Tfidf', num_methods)
        cmb_frm <- get_frm(name, granularity, 'combined', num_methods)
        bug_count_cmb_frm <- get_frm(name, granularity, 'bug_count_cmb', num_methods)
        sim_frm <- get_frm(name, granularity, 'similarity', num_methods)
        sim_predictions <- read_predictions(name, granularity)
        bugs_per_method <- get_bugs_per_method(stats)
        methods_with_bugs <- (bugs_per_method$`Methods with 1 bug` + bugs_per_method$`Methods with $\\geqslant$2 bugs`) / num_methods
        methods_with_multiple_bugs <- bugs_per_method$`Methods with $\\geqslant$2 bugs` / num_methods
        num_no_relevant <- sum(frm==num_methods+1)
        sim_num_no_relevant <- sum(sim_frm==num_methods+1)
        improvement <- frm - cmb_frm

        list=list(
                  stats=stats, 
                  method_ids=method_ids,
                  bugs_per_method=bugs_per_method, 
                  methods_with_bugs=methods_with_bugs,
                  methods_with_multiple_bugs=methods_with_multiple_bugs,
                  frm=frm, 
                  cmb_frm=cmb_frm, 
                  bug_count_cmb_frm=bug_count_cmb_frm, 
                  sim_frm=sim_frm, 
                  sim_predictions=sim_predictions,
                  improvement=improvement,
                  num_no_relevant=num_no_relevant,
                  sim_num_no_relevant=sim_num_no_relevant 
        )
    }

    argouml <- get_project('ArgoUML0.22', 'results', 'method_ids')
    jabref <- get_project('JabRef2.6', 'results', 'method_ids')
    jedit <- get_project('jEdit4.3', 'results', 'method_ids')
    mucommander <- get_project('muCommander0.8.5', 'results', 'method_ids')
@

<<echo=FALSE>>=
    best_frm <- min(argouml$frm, jabref$frm, jedit$frm, mucommander$frm)
    num_no_relevant_results <- argouml$num_no_relevant + jabref$num_no_relevant + jedit$num_no_relevant + mucommander$num_no_relevant 
    sim_num_no_relevant_results <- argouml$sim_num_no_relevant + jabref$sim_num_no_relevant + jedit$sim_num_no_relevant + mucommander$sim_num_no_relevant 

    projects <- data.frame(
        `Start Version` = c(
                               paste('0.20 (r', argouml$stats$start_rev, ')', sep=''),
                               paste('2.0 (r', jabref$stats$start_rev, ')', sep=''),
                               paste('4.2 (r', jedit$stats$start_rev, ')', sep=''),
                               paste('0.8.0 (r', mucommander$stats$start_rev, ')', sep='')
                            ),
        `End Version` = c(
                               paste('0.22 (r', argouml$stats$end_rev, ')', sep=''),
                               paste('2.6 (r', jabref$stats$end_rev, ')', sep=''),
                               paste('4.3 (r', jedit$stats$end_rev, ')', sep=''),
                               paste('0.8.5 (r', mucommander$stats$end_rev, ')', sep='')
                            ),
        `Evaluated Bugs` = c(argouml$stats$num_bugs, jabref$stats$num_bugs, jedit$stats$num_bugs, mucommander$stats$num_bugs),
        `Applicable Bugs` = c(argouml$stats$num_applicable_bugs, jabref$stats$num_applicable_bugs, jedit$stats$num_applicable_bugs, mucommander$stats$num_applicable_bugs),
        `Additional Training Bugs` = c(argouml$stats$num_train_bugs, jabref$stats$num_train_bugs, jedit$stats$num_train_bugs, mucommander$stats$num_train_bugs),
        `Methods` = c(argouml$stats$num_methods, jabref$stats$num_methods, jedit$stats$num_methods, mucommander$stats$num_methods),
        check.names=FALSE
    )
    rownames(projects) <- c('ArgoUML', 'JabRef', 'jEdit', 'muCommander')

    hits <- function(a) {
        c(sapply(c(1,10,100,1000,Inf), function(x) sum(a<=x, na.rm=TRUE)))
    }
    get_breaks <-function(frm, src) {
        breaks <- sapply(frm, hits)
        colnames(breaks) <- colnames(src)
        rownames(breaks) <- rownames(src)
        #breaks[nrow(breaks)-1,] <- src[nrow(src)-1,]
        breaks
    }

    breaks <- sapply(list(argouml$frm, jabref$frm, jedit$frm, mucommander$frm), hits)
    colnames(breaks)<-rownames(projects)
    rownames(breaks)<-c('1', '$\\leqslant$10', '$\\leqslant$100', '$\\leqslant$1000', 'Total')
    sim_breaks<-get_breaks(list(argouml$sim_frm, jabref$sim_frm, jedit$sim_frm, mucommander$sim_frm), breaks)
    cmb_breaks<-get_breaks(list(argouml$cmb_frm, jabref$cmb_frm, jedit$cmb_frm, mucommander$cmb_frm), breaks)
    bug_count_cmb_breaks<-get_breaks(list(argouml$bug_count_cmb_frm, jabref$bug_count_cmb_frm, jedit$bug_count_cmb_frm, mucommander$bug_count_cmb_frm), breaks)

    top1 <- sum(breaks[1,])
    cmb_top1 <- sum(cmb_breaks[1,])
    top10 <- sum(breaks[2,])
    cmb_top10 <- sum(cmb_breaks[2,])
    top100 <- sum(breaks[3,])
    cmb_top100 <- sum(cmb_breaks[3,])

    num_bugs <- argouml$stats$num_bugs + jabref$stats$num_bugs + jedit$stats$num_bugs + mucommander$stats$num_bugs
    methods_with_bugs <- range(argouml$methods_with_bugs, jabref$methods_with_bugs, jedit$methods_with_bugs, mucommander$methods_with_bugs)
    methods_with_multiple_bugs <- range(argouml$methods_with_multiple_bugs, jabref$methods_with_multiple_bugs, jedit$methods_with_multiple_bugs, mucommander$methods_with_multiple_bugs)
    num_projects <- 4
    percentage_applicable <- sum(argouml$stats$num_applicable_bugs, jabref$stats$num_applicable_bugs, jedit$stats$num_applicable_bugs, mucommander$stats$num_applicable_bugs) / sum(argouml$stats$num_bugs, jabref$stats$num_bugs, jedit$stats$num_bugs, mucommander$stats$num_bugs)
@

<<echo=FALSE>>=
    read_raw_results <- function(project, granularity, approach, id){
        ret <- read_file(c('method', 'predicted'), project, '/', granularity, '/', approach, '/', id)
        names(ret)[2] <- approach
        ret
    }

    read_sorted <- function(project, granularity, approach, id){
        result <- read_file(c('method', 'position'), project, '/', granularity, '/', approach, '/', id)
        ret <- result$position + 1
        names(ret) <- result$method
        ret
    }

    read_methods <- function(project, granularity, approach, id){
        read_file(c('method', 'bugs', 'tp', 'fp'), project, '/', granularity, '/', approach)
    }

    get_bug_analysis <- function(project, name, id){
        gold_set <- paste(results_dir, name, '/', 'GoldSets', '/', 'GoldSet', id, '.txt', sep='')
        methods_updated <- strsplit(system(paste('wc', '-l', gold_set), intern=TRUE), ' ')[[1]][1]

        results <- read_raw_results(name, 'results', 'relevant', id)
        results$sim <- read_raw_results(name, 'results', 'similarity', id)[,'similarity']
        results$direct <- read_raw_results(name, 'results', 'Tfidf', id)[,'Tfidf']
        results$cmb <- read_raw_results(name, 'results', 'combined', id)[,'combined']

        frm_pos <- project$frm[sapply(strsplit(names(project$frm), ' - '), `[[`, 1) == id]
        frm <- strsplit(names(frm_pos), ' - ')[[1]][2]
        updated_pos <- read_sorted(name, 'results', 'combined.sorted', id)[frm]
        cmb_frm_pos <- project$cmb_frm[sapply(strsplit(names(project$cmb_frm), ' - '), `[[`, 1) == id]
        cmb_frm <- strsplit(names(cmb_frm_pos), ' - ')[[1]][2]
        original_pos <- read_sorted(name, 'results', 'Tfidf.sorted', id)[cmb_frm]

        methods <- read_methods(name, 'results', 'similarity.methods')
        method <- methods[methods$method==cmb_frm,]

        list(
            `Project`=name,
            `Bug`=id,
            `Methods updated`=methods_updated,
            `FRM ID (Direct)`=frm,
            `FRM (Direct)`=project$method_ids[frm],
            `FRM Rank (Direct)`=frm_pos,
            `Updated position`=updated_pos,
            `Predicted methods`=sum(results$sim==1),
            `Relevant methods`=sum(results$sim==1 & results$relevant==1),
            `FRM ID (Combined)`=cmb_frm,
            `FRM (Combined)`=project$method_ids[cmb_frm],
            `FRM Rank (Combined)`=cmb_frm_pos,
            `Original position`=original_pos,
            `Method bugs`=method$bugs,
            `Method false positives`=method$fp,
            `Method true positives`=method$tp
        )
    }

    print_classifier <- function(details, key){
        cat('\\begin{samepage}\n')
        cat('\\begin{verbatim}\n')
        classifier <- paste(results_dir, details$`Project`, '/classifiers/', details$`Bug`, '_', details[key], sep='')
        x <- readLines(classifier)
        x <- x[grep('<|>', x)]
        writeLines(x)
        cat('\n\\end{verbatim}')
        cat('\n\\end{samepage}')
    }

    print_bug_analysis <- function(project, name, id){
        details <- get_bug_analysis(project, name, id)
        print(xtable(t(t(details)), align='ll'), table.placement='H')

        #print_classifier(details, 'FRM ID (Direct)')
        print_classifier(details, 'FRM ID (Combined)')
    }

    print_improved_bugs <- function(name, project, frm, sim, cmb, cmb_frm) {
        improved <- names(cmb_frm[cmb_frm<frm])
        for(i in improved){
            id <- strsplit(i, ' - ')[[1]][1]
            print_bug_analysis(project, name, id)
        }
    }

    escape <- function(x){
        x <- gsub('([_])', '\\\\\\\\\\1', x)
        x <- gsub('([.,])', '\\\\\\\\br{\\1}', x)
        x
    }

    argouml_bug_4364 <- get_bug_analysis(argouml, 'ArgoUML0.22', 4364)
    jedit_bug_1747300 <- get_bug_analysis(jedit, 'jEdit4.3', 1747300)
    jabref_bug_1540646 <- get_bug_analysis(jabref, 'JabRef2.6', 1540646)
@

<<echo=FALSE>>=
    fmt<-function(x, y=NULL, ndigits=0, app='\\\\%'){
        if(length(x)>1) {
            y<-x[2]
            x<-x[1]
        }
        x<-paste(round(x*100, ndigits), app, sep='')
        if(is.null(y)) {
           return(x)
        }
        y<-paste(round(y*100, ndigits), app, sep='')
        return(paste(x, y, sep='--'))
    }
@

